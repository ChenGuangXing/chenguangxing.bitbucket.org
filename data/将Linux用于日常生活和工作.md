﻿__将Linux用于日常生活和工作__
===========================

[TOC]

## 为什么要用Linux
目前我还是个Linux初学者, 作为一个门外汉我没法说Linux有多好.  简单介绍一下我用Linux的原因:  
1. 工作需要. 工作中需要用一些服务器, 开源又健壮的Linux当然是首选, 所以我要学习Linux应付工作.  
2. 开源可定制. 作为一个电脑爱好者, 谁不希望自己定制一个顺手的系统?  
3. 有点儿"装"的心理. 虽然我大部分电脑都有Windows授权, 但是只有我的第一台Win98电脑装的是原装系统, 后面买的电脑即使有正版授权也会装盗版, 甚至正版Win7也会重新分区装个盗版XP, 表面上好像跟微软"互不相欠"其实还是很心虚的. 一直盼着装个Linux然后默默的跟微软说:老子再也不用你的破软件了.  
4. 教育下一代. 希望看我儿子从小就用Linux系统, 不要像我这样"半路出家".  

## 怎样才能学好Linux?
## 将Linux用于日常生活和工作
## 常见问题
## 功能 速查
### 文件及文件夹管理
### 防火墙管理
 1)关闭防火墙-----service iptables stop 
 * 2)启动防火墙-----service iptables start 
 * 3)重启防火墙-----service iptables restart 
 * 4)查看防火墙状态--service iptables status 
 * 5)永久关闭防火墙--chkconfig iptables OFF 
 * 6)永久关闭后启用--chkconfig iptables ON 
 http://www.2cto.com/Article/201309/242182.html
http://www.cnblogs.com/ylh1223/archive/2012/05/31/2528517.html

### IP信息管理
### 启动管理
### 其它系统功能设置
#### 主机名管理

+ 查看主机名


```
hostname
或者:
uname -n
```

+ 修改主机名
    1. 第1步, 只是暂时修改, 重启后恢复成原主机名.
        ```
        hostname Server920
        ```
    2. 第2步, 只用于dns解析
    
        修改`/etc/hosts`文件中的, 例如: 
        ```        
        127.0.0.1 Server920
        ```
    3. 第3步, 真正意义上的修改主机名

        __CentOS__: 修改文件: `/etc/sysconfig/network`中的`hostname=`

        __Ubuntu__: 修改文件: `/etc/hostname`

    最后, `/etc/rc.d/boot.localnet start` 或重启计算机生效

参考资料:  
http://www.blogjava.net/alexwan/archive/2009/05/01/268526.html  
http://www.jb51.net/LINUXjishu/10958.html  
http://blog.csdn.net/ruglcc/article/details/7802077

## 推荐链接:
CentOS维护的一堆HowTo: http://wiki.centos.org/HowTos

# 准备安装

## 版本选择
工作中使用的服务器系统是CentOS, 生活中当首选它家亲戚Fedora. 目前最新版是20, 电脑配置不高, 所以选择用LXDE或Xfce版. 从网站排列顺序LXDE比较考前, 所以就选它了.  

题外话, 其实我在选Fedora前先试了下XUbuntu, 在一个奔腾M的老本上, 它显示CPU不支持PAE什么的错误. 彻底放弃.  

Fedora主页: http://fedoraproject.org/  
安装盘地址: http://download.fedoraproject.org/pub/fedora/linux/releases/20/Live/i386/Fedora-Live-LXDE-i686-20-1.iso

## 制作安装盘
光盘正在逐步淡出我们的生活且制作成本相对较高, 所以首选是U盘安装.  
U盘空间至少1G, 制作启动盘软件推荐 [Universal USB Installer][] 或 [LinuxLive USB Creator][].  

题外话, 我原来用过一个叫"U启动"的, 做启动盘的同时默认帮我做了量产什么的, 结果把U盘玩坏了.  
另外上面两个推荐的软件中[Universal USB Installer][]是绿色免安装的, 用着方便; [LinuxLive USB Creator][]虽然需要安装但是兼容性比别的好, 大家可以根据需要自己选择.  

软件都推荐了, 具体的制作方法就不介绍了, 通常不看说明就能操作.  

[Universal USB Installer]: http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/
[LinuxLive USB Creator]: http://www.linuxliveusb.com/  

# 安装系统
用电脑最重要的一点: __在做危险操作前一定要备份好重要的数据!!!!!!__    安装系统就属于危险操作.  

## U盘启动
U盘安装就需要U盘启动, 有的主板是设置好启动顺序, 插上U盘等着就行了; 有的主板是什么都不用设置, 启动的时候打开引导设备菜单, 选一个启动就行.  请根据自己的情况设置.  

## 和Windows并存
做人不能太自私, 虽然自己希望学Linux但是家人的感受还是要考虑的, 得留个Windows给别人, 所以最好能双启.  

我的硬盘原本是主分区(C盘)和扩展分区, 为了装Linux把扩展分区的数据拷到C盘, 然后把扩展分区删了再分成一个扩展分区和一个主分区.  
由于现在的Linux都认NTFS分区, 所以在扩展分区上建了一个逻辑盘, Windows/Linux都能用, 而最后一个主分区没做任何处理, 装Linux的时候再说.  

Fedora很聪明, 安装的时候主动把自己放到那个没做任何处理的主分区上, 并且在安装后自动生成了启动菜单, 开机后可以选择启动哪个系统.  

其实上面磨磨唧唧的说了一堆总结成一句话就是: 如果磁盘里已经有了Windows系统, Fedora会自动把自己安装在一个没被格式化的分区, 并制作启动菜单.  

# 将Linux用于日常生活和工作

## 常用命令
```
rd -rf SomeDir
yum install firefox
sudo find /usr -name xxx
```
新建用户: `adduser` 例如: `adduser user1`
设置用户密码: `passwd` 例如: `passwd user1`
查看当前OS是32位还是64位?
方法1:
```
getconf LONG_BIT
64

getconf LONG_BIT
32
```
方法2:
```
arch
i686

arch
x86_64
```
方法x:
`uname -a`, `file /bin/ls`, `cat /proc/cpuinfo | grep flags | grep ' lm ' | wc -l`

rpm的安装,更新和卸载
参考资料:http://blog.163.com/yang_jianli/blog/static/1619900062012912115552188/
安装: `rpm -ivh rpm文件`  
更新: `rpm -Uvh rpm文件`  
卸载: `rpm -e 软件名`, 强制删除软件加上 --nodeps  

yum安装和卸载
参考资料:http://blog.163.com/yang_jianli/blog/static/1619900062012912115552188/
安装: `yum install 服务名`
卸载: `yum remove 服务名`

安装VMware增强工具
VMwareTools-9.6.0-xxxxxxx.tar.gz
vmware-install.pl

## Grub2的启动顺序
前面制作了双系统, 默认自动启动Linux系统, 家人一个不小心就进去了, 所以需要调整成默认启动Windows系统.  


## 安装FireFox
为什么要装FireFox? 因为它跨平台, 书签/组件/设置方便同步. 虽然Chrome也非常棒,但是谷歌的东西在国内总是遇到各种连不上, 你懂的.  

安装Firefox只需要敲命令: `yum install firefox` 不但自动安装好软件而且将快捷方式放到开始菜单.  

卸载自带的midori浏览器: `yum erase midori`

怎么样? 是不是有点儿360软件管家的感觉了?  

## 安装VMware Tools
1. 如果VMware Tools的光盘没在虚拟机的光驱里, 单击`虚拟机`/`重新安装VMware Tools`.  
2. 复制光驱中的`VMwareTools-9.6.2-1688356.tar.gz`到硬盘  
3. 右击`VMwareTools-9.6.2-1688356.tar.gz`, `解压缩到此处`.  
4. 打开文件夹`vmware-tools-distrib`, 双击`vmware-install.pl`.  
5. 在弹出的菜单中选择`在终端中运行`, 然后一路回车, 安装结束.  

> 注意:
> VMwareTools-9.6.0及以前的版本需要系统安装 `ifconfig`, `gcc`, 及内核

## 挂载WMware的共享目录
前提1: 已经安装VMware Tools.  
前提2: 已经设置了共享目录.  

满足了前面两个前提, 那么`/mnt/hgfs`下面就是共享目录.  
参考资料: http://wenku.baidu.com/link?url=QWREBBN3bBd9mmWBdP8pnKjdibjISGdLfF3-Etu0n1vQJsZFbQTYDbxPIRKYZBdA80xERYLfA76PTgXA4y34BiihqIMWwXT2yx9o1svqWqu

## 用命令行下载文件
普通下载: `wget 文件URL地址`  
下载并改名: `wget -O wordpress.zip http://www.centos.bz/download.php?id=1080`  
限速下载: `wget –limit-rate=300k http://cn.wordpress.org/wordpress-3.1-zh_CN.zip`  
断点续传: `wget -c http://cn.wordpress.org/wordpress-3.1-zh_CN.zip`  
后台下载: `wget -b http://cn.wordpress.org/wordpress-3.1-zh_CN.zip`你可以使用以下命令来察看下载进度: `tail -f wget-log`  
测试链接: `wget –spider URL `  
下载多个文件: `wget -i filelist.txt`  

参考资料: http://www.jb51.net/LINUXjishu/86326.html

## 当前Linux所有用户列表
数据保存在: `/etc/passwd`, 用`awk -F: '{print $1}' /etc/passwd` 查询

## 修改用户密码
`passwd 用户名`

## 退出命令行
`exit`

## 文件和文件夹管理
删除一个文件  
`rm 文件名`

删除一个目录  
`rm -rf 目录名` 或 `rmdir 目录名`, rmdir只能删除空目录  

创建一个目录  
`mkdir`  

查找一个文件/目录
移动一个文件
移动一个文件夹
重命名文件/文件夹

## 解压缩
```
tar xvf MySQL-5.6.21-1.el7.x86_64.rpm-bundle.tar
tar -zxf git-1.7.2.2.tar.gz
tar -xjf xxx.tar.bz2
```

## 安装软件
多种安装方法:

1. 编译安装
2. yum安装
    `yum install libc.so.6`
3. rpm安装
    `rpm -ivh MySQL-server-5.6.21-1.el7.x86_64.rpm`
    `yum localinstall -y mysql-community-release-el6-3.noarch.rpm`
4. 

## 查找文件
参考资料: http://www.ruanyifeng.com/blog/2009/10/5_ways_to_search_for_files_using_the_terminal.html

##　安装英文系统, 看中文乱码(例如:CentOS5.8)
```
yum -y install fonts-chinese
yum -y install fonts-ISO8859-2
```
重启或
```
cd /usr/share/fonts/
fc-cache -fv
```
参考资料; http://www.linuxidc.com/Linux/2013-01/77362.htm

## 远程桌面
Fedora和CentOS上的远程桌面(Remote Desktop)是不是Windows中的mstsc, 是VNC. 
VNC客户端下载地址: http://www.realvnc.com/download/viewer/  
有固定IP可以用 `VNC` , 没固定IP可以用 `TeamViewer` .  

## 防火墙设置
```
CentOS 6 iptables 开放端口80 3306 22等

#/sbin/iptables -I INPUT -p tcp --dport 80 -j ACCEPT
#/sbin/iptables -I INPUT -p tcp --dport 22 -j ACCEPT
#/sbin/iptables -I INPUT -p tcp --dport 3306 -j ACCEPT
#/sbin/iptables -I INPUT -p tcp --dport 8080 -j ACCEPT
然后保存：
#/etc/init.d/iptables save
 
查看打开的端口：
# /etc/init.d/iptables status

极端情况

#关闭防火墙
/etc/init.d/iptables stop
```

## 可执行文件搜索路径(相当于Windows的Path)
在命令行敲一个命令后, 系统会按指定的顺序搜索这个命令, 如果没找到会显示 `-bash: php: command not found` . Windows的解决办法就是把这个路径加入到全局或当前用户的Path, Linux下将该路径放到 `.bash_profile` 里面, 例如: `export PATH=$PATH:/usr/local/php/bin`  
 `.bash_profile` 的位置在`/home/用户名`下面, 如果是root在 `/root\` 下面.  
修改不会立即生效, 解决办法查看对应章节.  
参考资料: http://www.maxwhale.cn/bash-php-command-not-found-in-ahm4-1/

## 修改 `.bash_profile` 立即生效
有下面三种方法: 
1 . .bash_profile
2 source .bash_profile
3 exec bash --login
参考资料: http://1984chenkailing.blog.163.com/blog/static/206375432013620112310543/

## Public key for XXX.rpm is not installed
`yum install XXX.rpm` 安装失败, 出现下列错误信息
```
warning: rpmts_HdrFromFdno: Header V4 DSA signature: NOKEY, key ID 98ab5139
Public key for XXX.rpm is not installed
```
解决办法: 
`yum install XXX.rpm  --nogpgcheck`
参考资料: http://blog.sina.com.cn/s/blog_716844910100qf8t.html

## configure: error: no acceptable C compiler found in $PATH
执行 `./configure` 时出现下列错误:  
```
configure: error: no acceptable C compiler found in $PATH
See `config.log' for more details.
```
安装 gcc解决. `yum install gcc`

参考资料: http://blog.csdn.net/duguduchong/article/details/8699774

## 安装git
最新版本到 http://git-scm.com/downloads 下载  

1. 安装依赖包: `yum install gcc curl curl-devel zlib-devel openssl-devel perl cpio expat-devel gettext-devel`  
2. 下载编译git:  
```sh
wget https://www.kernel.org/pub/software/scm/git/git-2.1.3.tar.gz
tar xzvf git-2.1.3.tar.gz
cd git-2.1.3
autoconf
./configure
make
make install
```
3. 验证版本: `git --version`

参考资料: http://www.ccvita.com/370.html

## 获得root权限
执行 `su root` 或 `su -`  
用 `exit` 退出

## 如何使用repo文件?
把*.repo放到`/etc/yum.repos.d/`
参考资料: http://blog.csdn.net/julius819/article/details/7381738

## 在CentOS5.8上用yum装git
1. 新建文件 `/etc/yum.repos.d/git.repo` 其内容是:
```
[epel]
name=Extra Packages for Enterprise Linux 5 - $basearch
#baseurl=http://download.fedoraproject.org/pub/epel/5/$basearch
mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=epel-5&arch=$basearch
failovermethod=priority
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL

[epel-debuginfo]
name=Extra Packages for Enterprise Linux 5 - $basearch - Debug
#baseurl=http://download.fedoraproject.org/pub/epel/5/$basearch/debug
mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=epel-debug-5&arch=$basearch
failovermethod=priority
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
gpgcheck=1

[epel-source]
name=Extra Packages for Enterprise Linux 5 - $basearch - Source
#baseurl=http://download.fedoraproject.org/pub/epel/5/SRPMS
mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=epel-source-5&arch=$basearch
failovermethod=priority
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL
gpgcheck=1
```
2. 执行 `yum install git git-daemon`  
3. 出现下列错误:  
```
warning: rpmts_HdrFromFdno: Header V3 DSA signature: NOKEY, key ID 217521f6

GPG key retrieval failed: [Errno 5] OSError: [Errno 2] No such file or directory
: '/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL'
```
执行下列命令行解决:  
```
cd /etc/pki/rpm-gpg
wget http://yum.csdb.cn/epel/RPM-GPG-KEY-EPEL
```
如果上面地址失效可以到 [这里](./将Linux用于日常生活和工作_md/RPM-GPG-KEY-EPEL) 下载
然后再 `yum install git git-daemon` 即可

参考资料: http://www.davegardner.me.uk/blog/2010/01/29/setting-up-git-on-centos-5-server/

## Fedora一键锁屏
套路: 首先, 找到锁屏的命令; 然后在`设置`/`键盘`/`应用程序快捷键`中加入快捷键.  
不同的XWindow软件锁屏的命令也不同, 我用的是Xfce, 锁屏命令: `xscreensaver-command -lock`.  
参考资料: 
http://unix.stackexchange.com/questions/101806/why-doesnt-my-screen-lock-in-xfce
https://ask.fedoraproject.org/en/question/41677/how-to-lock-the-screen-with-the-keyboard-in-fedora-20/

## 磁盘空间分配
暂时不知道怎样分区更科学, 但是可以看看系统默认是怎么做的:  
Fedora20(总大小40G) 默认 `/boot`  : 500M(ext4) ; 剩余 36.77G(lvm2) ;  
CentOS5.8(总大小160G) 默认 `/boot`  : 102M(ext3) ; 剩余 148.9G(lvm2) ;  

## 查看本机内存和虚拟内存
`free -m`

## U盘安装CentOS Minimal iso
1. 改名. 用`Universal-USB-Installer-1.9.5.5`做的启动U盘, 这玩意儿要求文件名中必须带`livecd`, 所以手动改ISO的名字, 加上`livecd`.  
2. 复制文件. 制作完U盘直接安装会显示错误信息`unable to read package metadata.this may be due to a missing repodata directory`, 原因是U盘`\repodata`下的文件没有扩展名. 解决办法是:删除U盘`\repodata`下的所有文件, 然后将光盘`/repodata`下的文件复制过来.  

参考资料: http://renzhenxing.blog.51cto.com/728846/1185859

## CentOS6设置IP信息
1. `ip a` 查看有哪些网络设备, 你可能看到:  
```
1:    lo: pula pula pula...
2:    eht0: pula  pula pula ......
```
2. 当看到 `2:    eth0:`说明有个叫`eth0`的网卡, 可以到 `/etc/sysconfig/network-scripts/` 找到它的配置文件 `ifcfg-eth0`.  
3. 配置文件常见的配置项包括:  
```
DEVICE=eth0
HWADDR=6C:62:6D:06:8E:97
TYPE=Ethernet
UUID=70012293-1d56-4d8c-91d7-1042b8983155
ONBOOT=yes
NM_CONTROLLED=yes
BOOTPROTO=static
IPADDR=10.0.6.125
NETMASK=255.255.255.0
GATEWAY=10.0.6.1
DNS1=114.114.114.114
DNS2=202.99.166.4
```
4. 设置完成后, 重启网络服务后生效. `service network restart`   

常用ip命令(ip命令设置重启后失效):  
| 命令 | 说明 |
| ------ | ------ |
| ip link show                                                                     | 显示网络接口信息 |
| ip link set eth0 up                                                          | 开启网卡 |
| ip link set eth0 down                                                     | 关闭网卡 |
| ip addr show                                                                   | 显示网卡IP信息 |
| ip addr add 192.168.0.1/24 dev eth0                       | 设置eth0网卡IP地址192.168.0.1 |
| ip addr del 192.168.0.1/24 dev eth0                         | 删除eth0网卡IP地址 |
| ip route add default via  192.168.0.254  dev eth0 | 设置默认网关为192.168.0.254 |
| ip route del default                                                         | 删除默认路由 |

参考资料: 
http://manual.blog.51cto.com/3300438/789229  
http://www.krizna.com/centos/how-to-setup-network-in-centos-6/

Ubuntu设置IP信息
编辑文件: `/etc/network/interfaces`, 并用下面的行来替换有关eth0的行: 
```
# The primary network interface
auto eth0
iface eth0 inet static
    pre-up ifconfig eth0 hw ether 00:00:10:00:09:25
    address 10.0.9.25
    netmask 255.255.255.0
    network 10.0.9.0
    broadcast 10.0.9.255
    gateway 10.0.9.1
    #broadcast 192.168.2.255
```
修改完成后, `sudo /etc/init.d/networking restart` 重启网络服务生效
参考资料: 
http://www.cnblogs.com/empire/archive/2011/01/10/1931877.html   
http://www.linuxidc.com/Linux/2013-01/77919.htm

## CentOS6Minimal安装xfce
```
# 64位系统
wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
# 32位系统
wget http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm

rpm -ivh epel-release-6-8.noarch.rpm

yum groupinstall "Xfce" "X Window System" "Fonts"
# yum install xorg-x11-fonts-Type1 xorg-x11-fonts-misc
```
 重启系统  

每次通过 `startxfce4` 或 `/sbin/telinit 5` 启动xfce. 或者自动启动xfce, 通过到 `/etc/inittab` 中找到 `id:3:initdefault:` 替换为 `id:5:initdefault:`  

参考资料: 
http://www.namhuy.net/475/how-to-install-gui-to-centos-minimal.html

## `startxfce4` 后, 出现`Could not look up internet address for myhost`
`startxfce4` 后, 出现一个不咬人各应人的对话框:  
```
Could not look up internet address for myhost. This will prevent XFCE from working correctly. 
It may be possible ti correct the problem by adding myhost to the file /etc/hosts on your system.
```
将本机IP和机器名写到`/etc/hosts`, 例如: `127.0.0.1 Server920`

## 安装VNC Server
安装VNC Server: `yum install tigervnc-server`  
手动启动VNC Server 服务: `vncserver`  

相关配置文件:  
`/etc/sysconfig/vncservers`  
`/用户目录/.vnc/xstartup`

### 连接到VNC服务器后, 只有背景桌面没有菜单
症状: CentOS6.6 Minimal 安装了 Xfce VNCServer , 客户端连接到VNC服务器后, 只有背景桌面没有菜单  
问题很可能出在`xstartup`文件上, 以root为例:  

1. 首先备份 `/用户目录/.vnc/xstartup`  
2. 修改`xstartup`的内容为:  
```
#!/bin/sh
/usr/bin/startxfce4
```

## OpenSSHServer
Fedora Xfce 20 默认没装 OpenSSHServer.  
安装很简单: `yum install openssh-server`  
开启服务: `service sshd start`  

## 安装JRE
可以选择安装 `tar.gz` 这样的绿色版或 `rpm` 这样的安装包.  
解压缩: `tar zxvf jre-7u<version>-linux-x64.tar.gz`  
RPM安装: `rpm -ivh jre-7u<version>-linux-x64.rpm`  
升级安装: `rpm -Uvh jre-7u<version>-linux-x64.rpm`  

## 安装SmartGit
是个绿色程序, 解压就能运行. 例如我们安装在`/opt`下面.  
直接运行`/opt/smartgit/bin/smartgit.sh`会报错, 提示需要安装JRE, 安装了JRE后(例如也安装在`/opt`下面), 还需要修改配置文件`/opt/smartgit/bin/.smartgit/smartgit.vmoptions`的内容为`jre=/opt/jre1.8.0_25/bin/java`, 并为java创建链接`ln -s /opt/jre1.8.0_25/bin/java /bin/java`. 这样就可以用了.  

## 安装Python
安装Python2: `yum install python2`  
安装Python3: `yum install python3`  
运行Python2: `python2`  
运行Python3: `python3`  

参考资料: https://docs.oracle.com/javase/7/docs/webnotes/install/linux/linux-jre.html

## 挂载和弹出光驱
挂载光驱: `mount /dev/cdrom /media`
弹出光驱: `eject`
参考资料: http://forum.ubuntu.org.cn/viewtopic.php?t=271395

## You must put some 'source' URIs in your sources.list
问题如下:  
```
~$ sudo apt-get build-dep gcc
[sudo] password for user:
Reading package lists... Done
Building dependency tree
Reading state information... Done
E: You must put some 'source' URIs in your sources.list
```
解决: 
编辑文件: `/etc/apt/sources.list`, 加入源:  
```
deb http://mirrors.163.com/ubuntu/ precise-updates main restricted
deb-src http://mirrors.163.com/ubuntu/ precise-updates main restricted
deb http://mirrors.163.com/ubuntu/ precise universe
deb-src http://mirrors.163.com/ubuntu/ precise universe
deb http://mirrors.163.com/ubuntu/ precise-updates universe
deb-src http://mirrors.163.com/ubuntu/ precise-updates universe
deb http://mirrors.163.com/ubuntu/ precise multiverse
deb-src http://mirrors.163.com/ubuntu/ precise multiverse
deb http://mirrors.163.com/ubuntu/ precise-updates multiverse
deb-src http://mirrors.163.com/ubuntu/ precise-updates multiverse
deb http://mirrors.163.com/ubuntu/ precise-backports main restricted universe multiverse
deb-src http://mirrors.163.com/ubuntu/ precise-backports main restricted universe multiverse
```

## bash:./vmware-install.pl :/usr/bin/perl:bad interpreter:No such file or directory.
bash:./vmware-install.pl :/usr/bin/perl:bad interpreter:No such file or directory.

如果出现这个提示，则表明系统没有安装Perl环境，可以使用如下命令来提供Perl支持
```
yum groupinstall "Perl Support"
```
参考资料: http://www.2cto.com/os/201111/112284.html

推荐资料:  
http://askubuntu.com/questions/496549/error-you-must-put-some-source-uris-in-your-sources-list
http://www.cnblogs.com/lyp3314/archive/2012/07/18/2597973.html

| net-tools | iproute2 |
| arp -na | ip neigh |
| ifconfig | ip link |
| ifconfig --help | ip help |
| ifconfig -s | ip -s link |
| ifconfig eth0 up | ip link set eth0 up |
| ipmaddr | ip maddr |
| iptunnel | ip tunnel |
| netstat | ss |
| netstat -i | ip -s link |
| netstat -g | ip maddr |
| netstat -l | ss -l |
| netstat -r | ip route |
| route add | ip route add |
| route del | ip route del |
| route -n | ip route show |
| vconfig | ip link |
