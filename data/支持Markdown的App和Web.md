支持Markdown的App和Web
======================

[TOC]

# App
|软件|介绍|平台|
|---|---|---|
| [Haroopad][] | 看起来丝毫不比MdCharm逊色, 但是目前是0.12版问题还很多,期望早日发布1.0 | Windows, MacOS, Linux |
| [MdCharm][] | 收费版, 但是可以无限次的免费试用, 业界良心! 也是我最常用的. | Windows, Linux |
| [Texts][] |  它隐藏了markdown语法,更像是传统的文字处理软件. 试用30天 | Windows |
| [MarkdownPad 2][] | 有免费版和收费版, 需要Microsoft .NET 4, 免费版速度慢,功能少,不好用; 收费版没用过 | Windows |
| [Notepad++][] | 无预览, 需要自己下插件 | Windows |
| [Sublime Text][] | 无预览, 需要自己下插件 | Windows |
| [MarkDown#Editor][] | 中文输入法总是出问题| Windows |
| [Mou][] | | MacOS
| [iA Writer][] | | MacOS, iPhone/iPhad |

[Haroopad]: http://pad.haroopress.com/
[Texts]: http://www.texts.io/
[MarkdownPad 2]: http://www.markdownpad.com/
[MdCharm]: http://www.mdcharm.com/
[Notepad++]: http://notepad-plus-plus.org/
[Sublime Text]: http://www.sublimetext.com/
[MarkDown#Editor]: http://hibara.org/software/markdownsharpeditor/#download
[Mou]: http://mouapp.com/
[iA Writer]: http://www.iawriter.com/

---

# Web
|网站|介绍|
|---|---|
| [小书匠](http://markdown.xiaoshujiang.com/) | |
| [Cmd](https://www.zybuluo.com/mdeditor) | |
| [Dillinger](http://dillinger.io/) | |
| [MaHua](http://mahua.jser.me/) | |
| [StackEdit](https://stackedit.io/) | 它的主页就是个非常好的例子! |
| [马克飞象](http://maxiang.info/) | 支持MathJax公式 |
| [Dillinger](http://dillinger.io/) | |

如果范围可以再扩大点儿的话:

https://bitbucket.org/  
https://github.com/  
http://jianshu.io/  
http://www.ituring.com.cn/
......

---

[博客首页](https://bitbucket.org/ChenGuangXing/blog/wiki/Home)  
> Update: 2014/06/17  
> Create: 2014/06/10  
