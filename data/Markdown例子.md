# 二级标题(Setext)
## 语法:
```
标题文本-我是二级标题的第一级
=
标题文本-我是二级标题的第二级
-
```
##示例:
标题文本-我是二级标题的第一级
=
标题文本-我是二级标题的第二级
-

---

# 六级标题(atx)
## 语法:
```
# 我是六级标题的第一级
## 我是六级标题的第二级
### 我是六级标题的第三级
#### 我是六级标题的第四级
##### 我是六级标题的第五级
###### 我是六级标题的第六级
```
##示例:
# 我是六级标题的第一级
## 我是六级标题的第二级
### 我是六级标题的第三级
#### 我是六级标题的第四级
##### 我是六级标题的第五级
###### 我是六级标题的第六级

---

# 自动根据二级或六级标题生成目录
## 语法:
```
[TOC]
```
1. TOC必须是大写. 
2. 它会自动把文档中的二级或六级标题生成目录,放在[TOC]所在的位置.

## 示例:
[TOC]

--------------------------------------------------------------------------------

# 分割线
## 语法:
```
(空行)
---

* * *

***

- - -
```
## 示例:
---

* * *

***

- - -

---
# 代码
## 语法:
```
 ```
 我是注释中的内容
 ```
```
## 示例:
```
我是注释中的内容
```
---
# 在一段话中嵌入代码
## 语法:
```
后面会嵌入一个函数 `printf()` 看到没?
```
## 示例:
后面会嵌入一个函数 `printf()` 看到没?

---
# 指定某种语言的代码
## 语法:
```
 ```
 #!python
def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
 ```
```
```
 ```C++
int main(){
    printf("Hello world!");
}
 ```
```
## 示例:
```
#!python
def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
```
```C++
int main(){
    printf("Hello world!");
}
```
---

# 无序列表
## 语法:
```
* 我是一个无序列表项
+ 我也是一个无序列表项
- 我还是一个无序列表项
```
## 示例:
* 我是一个无序列表项
+ 我也是一个无序列表项
- 我还是一个无序列表项

---
# 有序列表
## 语法:
```
1. 我是一个有序列表项
3. 你肯定发现了, 我的序号和实际的数字无关
100. 什么?你还没发现?
```
## 示例:
1. 我是一个有序列表项
3. 你肯定发现了, 我的序号和实际的数字无关
100. 什么?你还没发现?

---

# 脚注
## 语法:
```
你知道什么是脚注[^脚注]吗? 如果不知道, 点一下上标试试.有趣的是上标的数值不是按出现顺序而是在脚注列表中的顺序.  

Footnotes[^1] have a label[^label] and a definition[^!DEF].

[^1]: This is a footnote
[^label]: A footnote on "label"
[^!DEF]: The definition of a footnote.
[^脚注]: 脚注就是在页面脚丫子下面出现的标注,味道不错吧? 另外,点我后面可以回去呦~~~
```
## 示例:
你知道什么是脚注[^脚注]吗? 如果不知道, 点一下上标试试.有趣的是上标的数值不是按出现顺序而是在脚注列表中的顺序.  

Footnotes[^1] have a label[^label] and a definition[^!DEF].

[^1]: This is a footnote
[^label]: A footnote on "label"
[^!DEF]: The definition of a footnote.
[^脚注]: 脚注就是在页面脚丫子下面出现的标注,味道不错吧? 另外,点我后面可以回去呦~~~

---
# 斜体
## 语法:
```
*我是斜体*
_我也是斜体_
```
## 示例:
*我是斜体*   
_我也是斜体_

---
# 黑体
## 语法:
```
**我是黑体**
__我也是黑体__
```
## 示例:
**我是黑体**

__我也是黑体__

---
# 黑斜体
## 语法:
```
***我是黑斜体***
___我也是黑斜体___
```
## 示例:
***我是黑斜体***

___我也是黑斜体___

---
# 删除线
## 语法:
```
~~这样就删除了~~
```
## 示例:
~~这样就删除了~~

---
# 换行
## 语法:
```
有两种方法可以实现换行, 1.两段话中间有一个空行; 2.第一段话以至少2个空格结尾.  
貌似空行导致的换行的行间距比2个空格到只换行的行间距大呀
```
## 示例:
A: 我后面有两个空格哦~  
B: 真的吗?

A: 我后面没有空格,但下面有一个空行哦~~

B: Really?

---
# 直接链接
## 语法:
```
https://bitbucket.org/ChenGuangXing/markdowndemo/
<123@456.com>
```
## 示例:
https://bitbucket.org/ChenGuangXing/markdowndemo/


<123@456.com>

---
# 普通链接
## 语法:
```
[支持Markdown的App和Web](https://bitbucket.org/ChenGuangXing/markdowndemo/wiki/支持Markdown的App和Web "没错,点我就对了")
```
## 示例:
[支持Markdown的App和Web](https://bitbucket.org/ChenGuangXing/markdowndemo/wiki/支持Markdown的App和Web "没错,点我就对了")

---

# 参考式链接
## 语法:
```
目前, 常见的搜索引擎有:[谷歌][1], [必应][2], [DuckDuckGo][], 当然如果你没啥追求, 还可使用[百度][度娘]. 

[1]:　http://www.google.com/ "点我去谷歌"
[1]:　http://www.bing.com/ "点我有求必应"
[DuckDuckGo]:　http://www.DuckDuckGo.com/
[度娘]: http://www.baidu.com/
```
注意: `http://`是必加的, 否则会变成相对地址.

## 示例:
目前, 常见的搜索引擎有:[谷歌][1], [必应][2], [DuckDuckGo][], 当然如果你没啥追求, 还可使用[百度][度娘]. 

[1]: http://www.google.com/ "点我去谷歌"
[2]: http://www.bing.com/ "点我有求必应"
[DuckDuckGo]: http://www.DuckDuckGo.com/
[度娘]: http://www.baidu.com

---

# 链接到标题(BitBucket专用的语法?)
## 语法
```
[链到'支持Markdown的App和Web.md'的'Web'标题](https://bitbucket.org/ChenGuangXing/blog/wiki/Application_and_Skill/支持Markdown的App和Web.md#markdown-header-web)
```
注意:  
* 当标题是英文时, 直接把英文放到`markdown-header-`后面
* 当标题是中文时, 不能在`markdown-header-`后面不能是中文, 而是标题的序号(这个可用性不强)

## 示例
[链到'支持Markdown的App和Web.md'的'Web'标题](https://bitbucket.org/ChenGuangXing/blog/wiki/Application_and_Skill/支持Markdown的App和Web.md#markdown-header-web)

---

# 图片
## 语法:
```
![当你复制全文并作为文本粘贴时就会看到我了](/path/to/img.jpg "你看不到我~ 你看不到我~~")
![当你复制全文并作为文本粘贴时就会看到我了](https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png "好吧,我是Bitbucket的logo")
```
## 示例:
![当你复制全文并作为文本粘贴时就会看到我了](/path/to/img.jpg "你看不到我~ 你看不到我~~")  
![当你复制全文并作为文本粘贴时就会看到我了](https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png "好吧,我是Bitbucket的logo")

---
# 参考式图片
## 语法:
```
![当你复制全文并作为文本粘贴时就会看到我了][第一个图片]
![当你复制全文并作为文本粘贴时就会看到我了][第二个图片]

[第一个图片]: /path/to/img.jpg "你看不到我~ 你看不到我~~"
[第二个图片]: https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png "好吧,我是Bitbucket的logo"

```
## 示例:
![当你复制全文并作为文本粘贴时就会看到我了][第一个图片]  
![当你复制全文并作为文本粘贴时就会看到我了][第二个图片]

[第一个图片]: /path/to/img.jpg "你看不到我~ 你看不到我~~"
[第二个图片]: https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png "好吧,我是Bitbucket的logo"

---
# 带链接的图片
## 语法
```
[![Bitbucket](https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png)](http://Bitbucket.Org/)  
或  
[![image]][URL]  
[image]: https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png "Bitbucket"
[URL]: http://Bitbucket.Org/
```
(其实就是链接中显示文字的地方嵌入了一个图片)
## 示例
[![Bitbucket](https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png)](http://Bitbucket.Org/)  
或  
[![image]][URL]  
[image]: https://www.atlassian.com/en/wac/software/bitbucket/overview/productLogo/imageBinary/bitbucket_logo_landing.png "Bitbucket"
[URL]: http://Bitbucket.Org/

---
# 表格
## 语法:
```
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
```
其中, `:-------------:`代表居中对齐, `-----:`代表左对齐. 另外`-`至少要有三个
## 示例:
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

---
# 区块引用 Blockquotes
# 语法:
```
>我是区块引用
>你可以再每行都放`>`

> 也  
可以  
只在  
段首
放一个`>`

>>你肯定发现了, 
我支持嵌套, 和E-mail很像吧?
```
# 示例:
>我是区块引用
>你可以再每行都放`>`

> 也  
可以  
只在  
段首
放一个`>`

>>你肯定发现了, 
我支持嵌套, 和E-mail很像吧?

---

---

[博客首页](https://bitbucket.org/ChenGuangXing/blog/wiki)  
> Update: 2014/06/20  
> Create: 2014/06/10  